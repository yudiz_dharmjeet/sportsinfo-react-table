import { format } from "date-fns";

export const COLUMNS = [
  {
    Header: "Image",
    Footer: "Image",
    accessor: "sImage",
    Cell: (tableProps) => (
      <img src={tableProps.row.original.sImage} width={60} alt="SportsImage" />
    ),
  },
  {
    Header: "Title",
    Footer: "Title",
    accessor: "sTitle",
  },
  {
    Header: "Description",
    Footer: "Description",
    accessor: "sDescription",
  },
  {
    Header: "Type",
    Footer: "Type",
    accessor: "eType",
  },
  {
    Header: "Created At",
    Footer: "Created At",
    accessor: "dCreatedAt",
    Cell: ({ value }) => {
      return format(new Date(value), "dd/MM/yyyy");
    },
  },
  {
    Header: "View Counts",
    Footer: "View Counts",
    accessor: "nViewCounts",
  },
  {
    Header: "Comments Count",
    Footer: "Comments Count",
    accessor: "nCommentsCount",
  },
];
