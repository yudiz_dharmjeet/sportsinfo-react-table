import axios from "axios";
import React, { useState, useEffect } from "react";
import "./App.css";
import Table from "./components/Table";

function App() {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    axios
      .get("https://backend.sports.info/api/v1/posts/recent")
      .then((response) => setTableData(response.data.data))
      .catch((error) => setError(error.message));
    setLoading(false);
  }, []);

  if (loading) {
    return <h2>Loading...</h2>;
  }

  if (error) {
    return <h2>{error}</h2>;
  }

  return (
    <div className="app">
      <Table tableData={tableData} />
    </div>
  );
}

export default App;
